#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Run the built-in MkDocs development server. MkDocs will watch the project
# directory and rebuild the documentation when a change is detected. The
# documentation is available at http://localhost:<port> (default port is 8000).
#
# Examples:
#   ./serve.sh
#   ./serve.sh --name my-documentation --interface 0.0.0.0 --port 9000
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-utils4b/ndd-utils4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/variables.sh"



# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_string  "name"       "${DDIDIER_MKDOCS_CONTAINER_NAME}"               "The name of the container"                                            "n"
DEFINE_string  "interface"  "${DDIDIER_MKDOCS_CONTAINER_EXPOSED_INTERFACE}"  "The network interface on the host machine the container is bound to"  "i"
DEFINE_integer "port"       "${DDIDIER_MKDOCS_CONTAINER_EXPOSED_PORT}"       "The port on the host machine the container is bound to"               "p"
DEFINE_boolean "pdf"        false                                            "Enable PDF gneration"                                                 ""
DEFINE_boolean "debug"      false                                            "Enable debug mode"                                                    "d"

read -r -d '' FLAGS_HELP <<EOF
Run MkDocs in server mode. MkDocs will watch the project directory and rebuild
the documentation whenever a change is detected. The documentation will
be available at http://localhost:<port>. The default port is ${DDIDIER_MKDOCS_CONTAINER_EXPOSED_PORT}.
Examples:
  ./serve.sh
  ./serve.sh --name my-documentation --interface 0.0.0.0 --port 9000
EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        # shellcheck disable=SC2034
        ndd::logger::set_stdout_level "DEBUG"
    else
        # shellcheck disable=SC2034
        ndd::logger::set_stdout_level "INFO"
    fi

    local container_name="${FLAGS_name}"
    local exposed_interface="${FLAGS_interface}"
    local exposed_port="${FLAGS_port}"

    if [[ -z "${container_name}" ]]; then
        log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        log error "┃ The container name cannot be empty. Please use '-n' or '--name'"
        log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        exit 1
    fi

    if [[ -z "${exposed_interface}" ]]; then
        log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        log error "┃ The exposed interface cannot be empty. Please use '-i' or '--interface'."
        log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        exit 1
    fi

    if [[ -z "${exposed_port}" ]]; then
        log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        log error "┃ The exposed port cannot be empty. Please use '-p' or '--port'."
        log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        exit 1
    fi

    local docker_image="ddidier/mkdocs:${DDIDIER_MKDOCS_IMAGE_VERSION}"
    local source_directory="${PROJECT_DIR}"

    # Use the terminal if present
    # local container_uid="${UID}"
    # local container_username="${USER}"
    local container_uid
    local container_username
    container_uid="$(id -u)"
    container_username="$(id -u -n)"

    local container_network="${DDIDIER_MKDOCS_CONTAINER_NETWORK_NAME}"

    if ! docker network inspect "${container_network}" &> /dev/null; then
        docker network create "${container_network}"
    fi

    local current_container_network
    current_container_network="$(docker network inspect "${container_network}" --format "{{.Driver}}")"

    if [[ "${current_container_network}" != "bridge" ]]; then
        log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        log error "┃ The container network '${container_network}' must be a bridge but is '${current_container_network}'"
        log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        exit 1
    fi

    if [[ -L "${source_directory}" ]]; then
        log warning "The source directory cannot be a symbolic link."
        source_directory="$(realpath "${source_directory}")"
    fi

    local export_pdf=0

    if [[ "${FLAGS_pdf}" -eq "${FLAGS_TRUE}" ]]; then
        export_pdf=1
    fi

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ Serving the MkDocs documentation"
    log info "┃"
    log info "┃ - using the Docker image: ${docker_image}"
    log info "┃ - in the container: ${container_name}"
    log info "┃ - in the network: ${container_network}"
    log info "┃ - as the user: ${container_username}"
    log info "┃ - as the user ID: ${container_uid}"
    log info "┃ - listening on interface: ${exposed_interface}"
    log info "┃ - listening on port: ${exposed_port}"
    log info "┃"
    log info "┃ This container can usually be terminated with: Control+C"
    log info "┃ But if this does not work: docker kill ${container_name}"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    # Use the TTY if present
    local docker_run_interactive=""
    local docker_run_tty=""

    if [[ -t 1 ]]; then
        docker_run_interactive="-i"
        docker_run_tty="-t"
    fi

    # echo
    # docker run --rm -it                                           \
    #     ${docker_run_interactive}                                 \
    #     ${docker_run_tty}                                         \
    #     --name "${container_name}"                                \
    #     -e USER_ID="${container_uid}"                             \
    #     -e USER_NAME="${container_username}"                      \
    #     -p "${exposed_interface}:${exposed_port}:${exposed_port}" \
    #     -v "${source_directory}":/data                            \
    #     "${docker_image}"                                         \
    #         mkdocs serve                                          \
    #             --dev-addr "0.0.0.0:${exposed_port}"              \
    #             "$@"                                              \
    # || true

    if [[ "${PLANTUML_START}" == "1" ]] && ! docker container inspect "${PLANTUML_CONTAINER_NAME}" &> /dev/null; then
        docker run --rm                                             \
            --detach                                                \
            --name "${PLANTUML_CONTAINER_NAME}"                     \
            --publish "${PLANTUML_CONTAINER_EXPOSED_PORT}":8080     \
            --network "${container_network}"                        \
            plantuml/plantuml-server:"${PLANTUML_IMAGE_VERSION}"
    fi

    log debug "$(ndd::print::script_output_start)"
    docker run --rm                                               \
        ${docker_run_interactive}                                 \
        ${docker_run_tty}                                         \
        --name "${container_name}"                                \
        -e USER_ID="${container_uid}"                             \
        -e USER_NAME="${container_username}"                      \
        -e EXPORT_PDF=${export_pdf}             \
        -e ENABLE_PDF_EXPORT=${export_pdf}      \
        -p "${exposed_interface}:${exposed_port}:${exposed_port}" \
        -v "${source_directory}":/data                            \
        --network "${container_network}"        \
        "${docker_image}"                                         \
            mkdocs serve                                          \
                --dev-addr "0.0.0.0:${exposed_port}"              \
                "$@"                                              \
    || true
    log debug "$(ndd::print::script_output_end)"

    log info " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info " ┃ BUILD -- Success!                                                            "
    log info " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
