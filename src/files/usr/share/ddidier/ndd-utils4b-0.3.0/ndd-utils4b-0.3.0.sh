#!/usr/bin/env bash

# ==============================================================================
# NDD Utils4B -- Simple utility library for Bash.
#
# Copyright 2020 David DIDIER - All Rights Reserved
# Released under the MIT license (https://opensource.org/licenses/MIT)
#
# Author     - David DIDIER
# Repository - https://gitlab.com/ddidier/bash-ndd-utils4b
# Version    - @NDD_UTILS4B_VERSION@
# ------------------------------------------------------------------------------
#
# See https://gitlab.com/ddidier/bash-ndd-utils4b for more details.
#
# ==============================================================================



# Avoid sourcing this library more than one time
if [[ -n "${NDD_UTILS4B_SOURCED+x}" ]] && [[ ${NDD_UTILS4B_SOURCED} ]]; then
  return 0
fi

NDD_UTILS4B_SOURCED=true



# ==============================================================================
# Commons
# ==============================================================================

# ------------------------------------------------------------------------------
# Enable some extended error catching.
#
function ndd::base::catch_more_errors_on() {
  set -o pipefail  # trace ERR through pipes
  set -o errtrace  # trace ERR through 'time command' and other functions
  set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
  set -o errexit   # set -e : exit the script if any statement returns a non-true return value
}

# ------------------------------------------------------------------------------
# Disable some extended error catching.
#
function ndd::base::catch_more_errors_off() {
  set +o pipefail  # trace ERR through pipes
  set +o errtrace  # trace ERR through 'time command' and other functions
  set +o nounset   # set -u : exit the script if you try to use an uninitialised variable
  set +o errexit   # set -e : exit the script if any statement returns a non-true return value
}



# ------------------------------------------------------------------------------
# Print the stacktrace to stderr with function names, line numbers and file paths.
# If this function is called from another function, use the 'nested_level' argument.
#
# Arguments:
#   @nested_level (integer) - the first trace to print (default = O)
#
# Outputs:
#   Print the stacktrace to stderr
#
function ndd::base::print_stack_trace() {
  local nested_level=${1:-0}
  local first_trace=$nested_level
  local stack_depth=$(( ${#FUNCNAME[@]} - 1 ))

  for ((i=first_trace; i<stack_depth; i++)); do
    local function_name="${FUNCNAME[$((i + 1))]}"
    local line_number="${BASH_LINENO[$i]}"
    local file_path="${BASH_SOURCE[$((i + 1))]}"

    echo "    in ${function_name} ($(realpath "$file_path"):${line_number})" >&2
  done
}





# ==============================================================================
# Print
# ==============================================================================

# ------------------------------------------------------------------------------
# Print a delimiter marking the beginning of another script output.
#
# Outputs:
#   Print a delimiter to stdout
#
function ndd::print::script_output_start() {
  echo "▼ ~~~~~ start of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ▼"
}

# ------------------------------------------------------------------------------
# Print a delimiter marking the end of another script output.
#
# Outputs:
#   Print a delimiter to stdout
#
function ndd::print::script_output_end() {
  echo "▲ ~~~~~ end of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ▲"
}





# ==============================================================================
# Strings
# ==============================================================================

# ------------------------------------------------------------------------------
# Convert the given string to lowercase.
#
# Arguments:
#   @string - the string to convert
#
# Outputs:
#   Print the lowercase string to stdout
#
function ndd::strings::to_lower() {
  echo "${1}" | awk '{print tolower($0)}'
}

# ------------------------------------------------------------------------------
# Convert the given string to uppercase.
#
# Arguments:
#   @string - the string to convert
#
# Outputs:
#   Print the uppercase string to stdout
#
function ndd::strings::to_upper() {
  echo "${1}" | awk '{print toupper($0)}'
}





# ==============================================================================
# Symbols
# ==============================================================================

function ndd::symbols::_print() {
  [[ -z "${3}" ]] && echo "${1}" || echo "${1}${2}${3}"
}

function ndd::symbols::check_mark() {
  ndd::symbols::_print "✔️" "  " "${1:-}"
}
function ndd::symbols::cross_mark() {
  ndd::symbols::_print "❌" " " "${1:-}"
}
function ndd::symbols::exclamation_mark() {
  ndd::symbols::_print "❗" " " "${1:-}"
}


function ndd::symbols::success() {
  ndd::symbols::_print "✅" " " "${1:-}"
}
function ndd::symbols::failure() {
  ndd::symbols::_print "❌" " " "${1:-}"
}


function ndd::symbols::debug() {
  ndd::symbols::_print "🐜" " " "${1:-}"
}
function ndd::symbols::information() {
  ndd::symbols::_print "ℹ️" "  " "${1:-}"
}
function ndd::symbols::warning() {
  ndd::symbols::_print "⚠️" "  " "${1:-}"
}
function ndd::symbols::error() {
  ndd::symbols::_print "🔥" " " "${1:-}"
}
function ndd::symbols::fatal() {
  ndd::symbols::_print "💥" " " "${1:-}"
}


function ndd::symbols::gear() {
  ndd::symbols::_print "⚙️ " " " "${1:-}"
}


# function ndd::symbols::collision() {
#   ndd::symbols::_print "💥" " " "${1:-}"
# }
# function ndd::symbols::fire() {
#   ndd::symbols::_print "🔥" " " "${1:-}"
# }
# function ndd::symbols::no_entry() {
#   ndd::symbols::_print "⛔" " " "${1:-}"
# }
# function ndd::symbols::radioactive() {
#   ndd::symbols::_print "☢️" " " "${1:-}"
# }





# ==============================================================================
# Testing
#
# Requires shunit2 (https://github.com/kward/shunit2/)
#
# See pull request https://github.com/kward/shunit2/pull/115
# ==============================================================================

# ------------------------------------------------------------------------------
# Check that a string is empty.
#
# Arguments:
#   @string - the string to check
#
function assertStringIsEmpty() {
  assertEquals "" "${1}"
}

# ------------------------------------------------------------------------------
# Check that a string is not empty.
#
# Arguments:
#   @string - the string to check
#
function assertStringIsNotEmpty() {
  assertNotEquals "" "${1}"
}

# ------------------------------------------------------------------------------
# Check that a file exists.
#
# Arguments:
#   @string - the path of the file to check
#
function assertFileExists() {
  local file_path="${1}"
  if [[ ! -f "${file_path}" ]]; then
    fail "Expected the file '${file_path}' to exist, but it does not"
  fi
}

# ------------------------------------------------------------------------------
# Check that a file does not exist.
#
# Arguments:
#   @string - the path of the file to check
#
function assertFileDoesNotExist() {
  local file_path="${1}"
  if [[ -f "${file_path}" ]]; then
    fail "Expected the file '${file_path}' to not exist, but it does"
  fi
}

# ------------------------------------------------------------------------------
# Check that a directory exists.
#
# Arguments:
#   @string - the path of the directory to check
#
function assertDirectoryExists() {
  local directory_path="${1}"
  if [[ ! -d "${directory_path}" ]]; then
    fail "Expected the directory '${directory_path}' to exist, but it does not"
  fi
}

# ------------------------------------------------------------------------------
# Check that a directory does not exist.
#
# Arguments:
#   @string - the path of the directory to check
#
function assertDirectoryDoesNotExist() {
  local directory_path="${1}"
  if [[ -d "${directory_path}" ]]; then
    fail "Expected the directory '${directory_path}' to not exist, but it does"
  fi
}



# ==============================================================================
# Versions
# ==============================================================================

# ------------------------------------------------------------------------------
# Compare two semantic versions (i.e. X.Y.Z).
#
# Arguments:
#   @string - the first semantic version to compare
#   @string - the second semantic version to compare
#
# Outputs:
#   Returns true if the first semantic version is strictly greater than the second one, false otherwise.
#
function ndd::versions::is_greater() {
  local version1="${1}"
  local version2="${2}"
  [[ "${version1}" != "${version2}" ]] || return
  [[ "${version1}" == "$(printf "%s\n%s" "${version1}" "${version2}" | sort -r -V | head -n1)" ]]
}

# ------------------------------------------------------------------------------
# Compare two semantic versions (i.e. X.Y.Z).
#
# Arguments:
#   @string - the first semantic version to compare
#   @string - the second semantic version to compare
#
# Outputs:
#   Returns true if the first semantic version is greater or equal to the second one, false otherwise.
#
function ndd::versions::is_greater_or_equal() {
  local version1="${1}"
  local version2="${2}"
  [[ "${version1}" == "${version2}" ]] && return
  [[ "${version1}" == "$(printf "%s\n%s" "${version1}" "${version2}" | sort -r -V | head -n1)" ]]
}

# ------------------------------------------------------------------------------
# Compare two semantic versions (i.e. X.Y.Z).
#
# Arguments:
#   @string - the first semantic version to compare
#   @string - the second semantic version to compare
#
# Outputs:
#   Returns true if the first semantic version is strictly less than the second one, false otherwise.
#
function ndd::versions::is_less() {
  local version1="${1}"
  local version2="${2}"
  [[ "${version1}" != "${version2}" ]] || return
  [[ "${version1}" == "$(printf "%s\n%s" "${version1}" "${version2}" | sort -V | head -n1)" ]]
}

# ------------------------------------------------------------------------------
# Compare two semantic versions (i.e. X.Y.Z).
#
# Arguments:
#   @string - the first semantic version to compare
#   @string - the second semantic version to compare
#
# Outputs:
#   Returns true if the first semantic version is less or equal to the second one, false otherwise.
#
function ndd::versions::is_less_or_equal() {
  local version1="${1}"
  local version2="${2}"
  [[ "${version1}" == "${version2}" ]] && return
  [[ "${version1}" == "$(printf "%s\n%s" "${version1}" "${version2}" | sort -V | head -n1)" ]]
}





# Used to test multiple sourcing
# shellcheck disable=SC2034
TEST_NDD_UTILS4B_SOURCED=true
