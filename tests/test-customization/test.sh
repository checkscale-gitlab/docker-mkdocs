#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    log info "Testing the project customization"

    # ---------- create temporary directories

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-mkdocs)
    temp_dir_link="$(dirname "${temp_dir}")/docker-mkdocs"

    rm -rf "${temp_dir_link}"
    ln -sf "${temp_dir}" "${temp_dir_link}"

    log info "Test data will be stored in the temporary directory: ${temp_dir}"
    log info "Use the following link for convenience: ${temp_dir_link}"

    # ----------

    log info "Copying default project files"
    cp -r "${PROJECT_DIR}"/tests/test-new-project/expected/* "${temp_dir}/"

    log info "Copying custom project files"
    cp -r "${PROJECT_DIR}"/tests/test-customization/source/* "${temp_dir}/"

    log info "Enabling PlantUML server"
    sed -i 's/^export PLANTUML_START=0/export PLANTUML_START=1/' "${temp_dir}/bin/variables.sh"

    # ----------

    # Tag the Docker image for testing
    log info "Tagging image 'ddidier/mkdocs:latest' with 'ddidier/mkdocs:testing'"
    docker tag "ddidier/mkdocs:latest" "ddidier/mkdocs:testing"

    log info "Generating HTML documentation"
    log debug "$(ndd::print::script_output_start)"
    while read -r line; do
        log debug "${line}"
    done < <( "${temp_dir}/bin/build-html.sh" )
    log debug "$(ndd::print::script_output_end)"

    # ----------

    log info "Checking differences between generated and expected files"

    local expected_dir="${PROJECT_DIR}/tests/test-customization/expected"

    rm -f "${temp_dir}/site/sitemap.xml.gz"
    sed -i -E 's/[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}/TESTED_IN_SCRIPT/g' "${temp_dir}/site/sitemap.xml"

    local delta
    delta=$(diff -r --exclude ".git" "${temp_dir}/site" "${expected_dir}/site" || true)

    if [[ "${delta}" != "" ]]; then
        log error "Found differences between generated and expected files"
        log debug "$(ndd::print::script_output_start)"
        log debug "${delta}"
        log debug "$(ndd::print::script_output_end)"
        log error "Some useful commands:"
        log error "  diff -r --exclude '.git' '${temp_dir_link}/site' '${expected_dir}/site'"
        log error "  meld '${temp_dir_link}/site' '${expected_dir}/site' &"
        log error "Project customization test: FAILED"

        # shellcheck disable=SC2154
        if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
            meld "${temp_dir_link}/site" "${expected_dir}/site" &
        fi

        exit 1
    fi

    # ----------

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ Project customization test: SUCCESS"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
