#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Package the MkDocs documentation.
#
# Examples:
#   ./package
#   ./package --name my-documentation --version 1.2.3
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-utils4b/ndd-utils4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/variables.sh"



# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_string  "name"     "${DDIDIER_MKDOCS_PROJECT_NAME}"  "The name part of the file name"     "n"
DEFINE_string  "version"  ""                                "The version part of the file name"  "v"
DEFINE_boolean "debug"    false                             "Enable debug mode"                  "d"

read -r -d '' FLAGS_HELP <<EOF
Package the generated documentation in the archive 'dist/\${name}-\${version}.tar.gz'.
Examples:
  ./package.sh --name my-documentation --version 1.2.3
EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

  if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
    # shellcheck disable=SC2034
    ndd::logger::set_stdout_level "DEBUG"
  else
    # shellcheck disable=SC2034
    ndd::logger::set_stdout_level "INFO"
  fi

  local archive_name="${FLAGS_name}"

  if [[ -z "${archive_name}" ]]; then
    log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log error "┃ The name part cannot be empty. Please use '-n' or '--name'"
    log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    exit 1
  fi

  local archive_version="${FLAGS_version}"

  # if [[ -z "${archive_version}" ]]; then
  #   log info "No version provided."
  #   if command -v git &> /dev/null; then
  #     if [[ -d "${PROJECT_DIR}/.git" ]]; then
  #       if git -C "${PROJECT_DIR}" diff --quiet &> /dev/null; then
  #         log info "Git repository does exist. Using commit hash as version."
  #         archive_version="$(git -C "${PROJECT_DIR}" rev-parse HEAD)"
  #       else
  #         log info "Git repository is dirty. Using 'snapshot' as version."
  #         archive_version="snapshot"
  #       fi
  #     else
  #       log info "Git repository does not exist. Using 'snapshot' as version."
  #       archive_version="snapshot"
  #     fi
  #   else
  #     log info "Git is not available. Using 'snapshot' as version."
  #     archive_version="snapshot"
  #   fi
  # fi
  if [[ -z "${archive_version}" ]]; then
    log info "No version provided. Using 'snapshot' as version."
      archive_version="snapshot"
  fi

  local archive_file_name="${archive_name}-${archive_version}.tar.gz"
  local distribution_dir="${PROJECT_DIR}/dist"

  # ----------

  log info "Packaging documentation in '${distribution_dir}/${archive_file_name}'"

  rm -rf "${distribution_dir:?}/"

  mkdir "${distribution_dir}/"

  cp -r "${PROJECT_DIR}/site/" "${distribution_dir}/${archive_name}-${archive_version}/"

  local tar_options="cfz"

  if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
    tar_options="${tar_options}v"
  fi

  log debug "$(ndd::print::script_output_start)"
  tar $tar_options "${distribution_dir}/${archive_file_name}" -C "${distribution_dir}/" "${archive_name}-${archive_version}"
  log debug "$(ndd::print::script_output_end)"

  log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log info "┃ PACKAGE -- Success!                                                           "
  log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
  local error_code="$?"

  test $error_code == 0 && return;

  log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

  exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
