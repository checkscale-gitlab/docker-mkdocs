#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Test the 'ddidier/mkdocs' Docker image.
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

ndd::base::catch_more_errors_on

ndd::logger::set_stdout_level "INFO"



function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"
    log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log error "┃ TESTS -- Failed!                                                             "
    log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR



"${PROJECT_DIR}/tests/test-image/test.sh" "${@}"
"${PROJECT_DIR}/tests/test-new-project/test.sh" "${@}"
"${PROJECT_DIR}/tests/test-customization/test.sh" "${@}"
"${PROJECT_DIR}/tests/test-pdf/test.sh" "${@}"
"${PROJECT_DIR}/tests/test-package/test.sh" "${@}"

log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
log info "┃ TESTS -- Perfect!                                                            "
log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

exit 0
