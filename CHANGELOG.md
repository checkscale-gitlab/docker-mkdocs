
# NDD Docker MkDocs

## Version 8.2.13-1

- feat: update libraries including Material for MkDocs v8.2.13

## Version 8.2.8-1

- feat: update libraries including Material for MkDocs v8.2.8

## Version 8.1.11-1

- ci: fix CI pipeline
- feat: remove PDF printer implementations since they are not working
- feat: change initial Git branch name from 'master' to 'main'
- feat: reduce image size
- feat: replace embedded UML server by a Docker container
- feat: add other PDF printer implementations
- feat: update libraries including Material for MkDocs v8.1.11

## Version 7.1.1-1

- feat: update libraries
- test: add log messages
- fix: container name when building PDF is now different
- test: fix PDF generation test
- docs: improve README
- fix: change how scripts query git repositories
- style: change the Docker mount point from "/docs" to "/data"
- feat: change the default target of the Makefile from "pdf" to "html"
- style: improve source code

## Version 6.2.7-1

- Make new project creation more verbose
- Improve output of helper scripts
- Add helper script "package.sh"
- Rename helper scripts
- Update libraries
- Reduce Docker image size

## Version 6.1.6-3

- Fix missing Graphviz dependency

## Version 6.1.6-2

- Update customization of mkdocs.yml (PDF)
- Update ndd-utils4b library
- Improve PDF generation
- Improve customization of new projects
- Update entrypoint with an official release
- Remove test-with-gitlab-runner.sh (extracted)
- Update ndd-log4b library
- Replace direct references with environment in shebangs
- Update documentation

## Version 6.1.6-1

- Initial release
