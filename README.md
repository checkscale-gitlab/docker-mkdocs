    
# NDD Docker MkDocs

<!-- MarkdownTOC levels=1,2,3 -->

1. [Introduction](#introduction)
1. [Installation](#installation)
    1. [From source](#from-source)
    1. [From Docker Hub](#from-docker-hub)
1. [Usage](#usage)
    1. [Initialisation](#initialisation)
    1. [Non interactive mode](#non-interactive-mode)
    1. [Interactive mode](#interactive-mode)
1. [Configuration](#configuration)
    1. [Bundled extensions](#bundled-extensions)
    1. [Add new extensions](#add-new-extensions)
1. [Development](#development)
    1. [Coding](#coding)
    1. [Releasing](#releasing)

<!-- /MarkdownTOC -->



<a id="introduction"></a>
## Introduction

The [Material for MkDocs] generator with extensions, plugins and custom utilities.

The versioning scheme of this Docker image is `<MATERIAL_FOR_MKDOCS_VERSION>-<DOCKER_IMAGE_VERSION>`.
For example, `8.2.13-1` stands for the first version of this Docker image using Material for MkDocs `8.1.11`.

Besides the [Material for MkDocs] generator, this image contains and is configured for the following extensions:

- [mkdocs-awesome-pages-plugin]
- [mkdocs-build-plantuml-plugin]
- [mkdocs-enumerate-headings-plugin]
- [mkdocs-git-revision-date-localized-plugin]
- [mkdocs-git-revision-date-plugin]
- [mkdocs-macros-plugin]
- [mkdocs-pdf-export-plugin]
- [mkdocs-plugin-progress]
- [mkdocs-with-pdf]
- [mkpdfs-mkdocs-plugin]
- [plantuml-markdown]

This image also contains some utility scripts to make life easier.

References:

- image on Docker Hub : https://hub.docker.com/r/ddidier/mkdocs
- sources on GitLab : https://gitlab.com/ddidier/docker-mkdocs

The image is based on the official [Material for MkDocs image](https://hub.docker.com/r/squidfunk/mkdocs-material/).



<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

The sources are available on [GitLab](https://gitlab.com/ddidier/docker-mkdocs).

```shell
git clone git@gitlab.com:ddidier/docker-mkdocs.git
cd docker-mkdocs
make
```

<a id="from-docker-hub"></a>
### From Docker Hub

The Docker image is available on [Docker Hub](https://hub.docker.com/r/ddidier/mkdocs)

```shell
export NDD_MKDOCS_VERSION="8.2.13-1"
docker pull ddidier/mkdocs:"${NDD_MKDOCS_VERSION}"
```



<a id="usage"></a>
## Usage

The documentation directory on the host is called `${HOST_DATA_DIR}` in the remainder of this document.

The directory `${HOST_DATA_DIR}` on the host must be mounted as a volume under `/data` in the container.
Use `-v "${HOST_DATA_DIR}":/data` to use a specific documentation directory.
Use `-v "${PWD}":/data` to use the current directory as the documentation directory.

[MkDocs] is executed inside the container by a custom user who is created by the custom Docker entry point.
You **must** pass to the container the environment variable `USER_ID` set to the UID of the user the files will belong to.
This is the ``-e USER_ID="${UID}" `` part in the examples of this documentation.

ℹ️ **Helper scripts are provided which already take care of this plumbing.**

<a id="initialisation"></a>
### Initialisation

[MkDocs] provides the `new` command to create the skeleton of a project.
The custom entrypoint extends this command to customize the generated project.

⚠️ **The directory `${HOST_DATA_DIR}` must already exist, otherwise the script will fail!**

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs new .
```

You should now customize the content of `bin/variables.sh`, mostly the project name and the exposed port.

<a id="non-interactive-mode"></a>
### Non interactive mode

The so-called *non-interactive mode* is when you issue commands directly from the host.

<a id="helper-scripts"></a>
#### Helper scripts

ℹ️ **Helper scripts are provided to help you with common tasks and are the recommanded way to go.**

They are located in the `bin` directory of the generated project.
They are configured with default variables located in the `bin/variables.sh` file.
Each one of them can be called with `--help` to display all the available options.

To generate the HTML documentation, call:

```shell
./bin/build-html.sh
```

To generate the HTML documentation, and watch for changes with live reload, call:

```shell
# use the default port (i.e. 8000)
./bin/serve.sh

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
./bin/serve.sh --port 12345

# use --help to see all the available options
./bin/serve.sh --help
```

To generate the HTML and the PDF documentations, call:

```shell
./bin/build-pdf.sh --pdf
# or
./bin/serve.sh --pdf
```

ℹ️ **And to make your life even a little easier, a `Makefile` is also generated at the root of your project.**

Usage is really simple at the expanse of not being customizable:

- to generate the HTML documentation, just call `make html`
- to generate the HTML and the PDF documentations, just call `make pdf`
- to generate and serve the HTML documentation, just call `make serve`
- to package the documentation, just call `make package`

<a id="docker-commands"></a>
#### Docker commands

You can of course directly use the [MkDocs] commands without relying on the helper scripts.

To generate the HTML documentation, call `mkdocs build`:

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs build
```

To generate the HTML documentation, and watch for changes with live reload, call `mkdocs serve`:

```shell
# use the default port (i.e. 8000)
docker run -it --rm -v "${HOST_DATA_DIR}":/data -p 8000:8000 -e USER_ID="${UID}" ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs serve
#                                                  ^^^^
#                                                  open your browser at http://localhost:8000/

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
docker run -it --rm -v "${HOST_DATA_DIR}":/data -p 12345:12345 -e USER_ID="${UID}" ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs serve --dev-addr "0.0.0.0:12345"
#                                                  ^^^^^                                                                                                   ^^^^^
#                                                  open your browser at http://localhost:12345/                                            customize server port
```

To generate the HTML and the PDF documentations, call `mkdocs build`:

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" -e EXPORT_PDF=1 ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs build
#                                                                   ^^^^^^^^^^^^^^^
# or
docker run -it --rm -v "${HOST_DATA_DIR}":/data -p 8000:8000 -e USER_ID="${UID}" -e EXPORT_PDF=1 ddidier/mkdocs:"${NDD_MKDOCS_VERSION}" mkdocs serve
#                                                                                ^^^^^^^^^^^^^^^
```

<a id="interactive-mode"></a>
### Interactive mode

The so-called *interactive mode* is when you issue commands from inside the container.

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/mkdocs:"${NDD_MKDOCS_VERSION}"
```

You should now be in the `/data` directory, otherwise just `cd` to `/data`.

You can now use [MkDocs] in the original way:

- to generate the HTML documentation, call `mkdocs build`
- to generate the HTML and the PDF documentations, call `EXPORT_PDF=1 mkdocs build`
- to generate and serve the HTML documentation, call `mkdocs serve`
- to generate and serve the HTML and the PDF documentations, call `EXPORT_PDF=1 mkdocs serve`



<a id="configuration"></a>
## Configuration

The default generated `mkdocs.yml` has been augmented with some (commented out) settings.

<a id="bundled-extensions"></a>
### Bundled extensions

This image comes with a number of already bundled extensions.

In most cases:

- to enable a bundled extension, simply uncomment the associated line in your `mkdocs.yml`
- to disable a bundled extension, simply comment the associated line in your `mkdocs.yml`

But some extensions require a little more work:

#### PlantUML #1

The default behaviour of the [plantuml-markdown] plugin is to render diagrams using a remote server.
A PlantUML container will be started by `./bin/build.sh` and `./bin/serve.sh` (or the corresponding `make` targets) if `PLANTUML_START` is set to `1` in `./bin/variables.sh`.

In your configuration file `mkdocs.yml` add or uncomment:

```yaml
markdown_extensions:
  - plantuml_markdown:
      server: http://plantuml:8080
```

#### PlantUML #2

If you don't like inline diagrams or embedded images, this Docker image comes with [mkdocs-build-plantuml-plugin].

In your configuration file `mkdocs.yml` add or uncomment:

```yaml
plugins:
  - build_plantuml:
      render: server
      server: http://plantuml:8080
```

<a id="add-new-extensions"></a>
### Add new extensions

If you want to use an extension which is not already bundled with this image, you need to:

1. create a new `Dockerfile` extending the `ddidier/mkdocs` image
2. install the Python package(s) of the extension(s) inside the `Dockerfile`
3. reference the extension(s) in the `mkdocs.yml` configuration file

```docker
#
# Dockerfile
#
FROM ddidier/mkdocs:latest

RUN pip install 'a-mkdocs-extension       == A.B.C' \
                'another-mkdocs-extension == X.Y.Z'
```



<a id="development"></a>
## Development

<a id="coding"></a>
### Coding

The `Makefile` provide some useful targets:

- `make setup` to install the development tools
- `make build` to build the Docker image
- `make quality` to run the quality checks ([ShellCheck])
- `make test` to run the tests
- `make` is the default target and is a shortcut to `make build quality test`

⚠️ **Do not forget to rebuild the Docker image before testing!**

<a id="releasing"></a>
### Releasing

Docker Hub doesn't provide an API to update the README anymore:
when a release has been created, don't forget to manually update the README on Docker Hub.





[Material for MkDocs]: https://squidfunk.github.io/mkdocs-material/
[MkDocs]: https://www.mkdocs.org/
[PlantUML]: https://plantuml.com/

[mkdocs-awesome-pages-plugin]: https://pypi.org/project/mkdocs-awesome-pages-plugin/
[mkdocs-build-plantuml-plugin]: https://pypi.org/project/mkdocs-build-plantuml-plugin/
[mkdocs-enumerate-headings-plugin]: https://pypi.org/project/mkdocs-enumerate-headings-plugin/
[mkdocs-git-revision-date-localized-plugin]: https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/
[mkdocs-git-revision-date-plugin]: https://pypi.org/project/mkdocs-git-revision-date-plugin/
[mkdocs-macros-plugin]: https://pypi.org/project/mkdocs-macros-plugin/
[mkdocs-pdf-export-plugin]: https://pypi.org/project/mkdocs-pdf-export-plugin/
[mkdocs-plugin-progress]: https://pypi.org/project/mkdocs-plugin-progress/
[mkdocs-with-pdf]: https://pypi.org/project/mkdocs-with-pdf/
[mkpdfs-mkdocs-plugin]: https://pypi.org/project/mkpdfs-mkdocs/
[plantuml-markdown]: https://pypi.org/project/plantuml-markdown/
