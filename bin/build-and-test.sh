#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Build the 'ddidier/mkdocs' Docker image then run all the tests.
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

ndd::base::catch_more_errors_on

"${PROJECT_DIR}/bin/build.sh" "${@}"
"${PROJECT_DIR}/bin/test.sh"  "${@}"
