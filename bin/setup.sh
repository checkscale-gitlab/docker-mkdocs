#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Setup the environment for the development of the 'ddidier/mkdocs' Docker image.
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

ndd::base::catch_more_errors_on

ndd::logger::set_stdout_level "INFO"



# ---------- Docker, of course:

if ! command -v docker > /dev/null; then
  log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log error "┃ This setup script requires Docker to be installed!                           "
  log error "┃ See https://www.docker.com/ for more details.                                "
  log error "┃ Aborting...                                                                  "
  log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

  exit 1
fi



# ---------- The ShellCheck Docker image is really slow:

SHELLCHECK_VERSION=0.7.1

log info "Installing ShellCheck ${SHELLCHECK_VERSION}"

mkdir -p "${PROJECT_DIR}/lib"

curl -L "https://github.com/koalaman/shellcheck/releases/download/v${SHELLCHECK_VERSION}/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz" \
  --output "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"

tar -xf "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz" -C "${PROJECT_DIR}/lib/"

rm -f "${PROJECT_DIR}/lib/shellcheck"

ln -sf "shellcheck-v${SHELLCHECK_VERSION}" "${PROJECT_DIR}/lib/shellcheck"

rm -f "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"



# ----------

log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
log info "┃ ENVIRONMENT SETUP -- Done!                                                   "
log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

exit 0
